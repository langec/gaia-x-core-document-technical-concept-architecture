# Example Gaia-X Participant Use Cases

The goal of this section is to illustrate how the Consumers, Federators
and Providers described in the conceptual model can appear in the real word.
This section focuses on the most typical kinds of actors and the list is
**not exhaustive**. Examples of Gaia-X Use Cases can be found in the [position paper published by the Dataspace Business Committee](https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf).

## Provider Use Cases

This section describes typical kinds of commonly known actors that have the Provider
role in Gaia-X. In general, all kinds of Resources can have their respective Provider.

* Providing various cloud services
    * which acts as Provider of a Service Offering consisting mostly of the Resource Software and Node. _Example: a Software-as-a-Service product_
* Providing data sets
    * a Provider who is mainly concerned with Data as a Resource and additional Software Resources necessary for enabling data sharing and usage control as well as monitoring. Example: _A set of data to train a machine learning algorithm_
* Providing Software 
    * a Provider who offers single or combined software Services. _Example: A domain-specific tool for data manipulation or analysis._
* Providing interconnection & networking services 
    * a Provider who offers standard and elevated Interconnection Resources that can go beyond the capacities of the regular Internet connection and exhibit special characteristics. _Example: Interconnection as a Service with special guarantees of bandwidth, latency, availability or security-related settings._



## Consumer Use Cases

This section gives examples of different Gaia-X Consumer scenarios, where the
Consumer in general can consume all kinds and combinations of Resources analog to the Provider use cases.

* Consuming software
    * A Software service can comprise a broad range of services. They range from cloud services, to high-performance computing services, data-driven software applications, or compositions of different kinds of services.
* Consuming data sets
    * A consumer of data sets may consume raw or processed data and use it as input for own software or combine it with Gaia-X software.
* Consuming interconnection & networking services
    * Consumers can obtain interconnection services as a stand-alone resource, or combine them with other services, for example, to improve stability of connections between different Nodes.
* Consuming storage or computing capacity
    * Consumers can make use of Gaia-X Nodes and combine them with other Gaia-X Resources.



## Federator Use Cases

The different Federators are not distinguished as being either domain-specific or cross-domain. Only accordance to the Policy Rules and operating according to the conditions mentioned in the Operating Model, including the respective conformity assessments and trust mechanisms, distinguish whether it is an ecosystem federated by Gaia-X or not.

* Federator of a Gaia-X Ecosystem
    * A Gaia-X Ecosystem is approved if all Federators comply to Gaia-X Policy Rules, and Federation Services fulfil certain criteria and conformity and trust assessment are perfomed as specified by the Gaia-X Assocaition AISBL. In this case, any entity has the option to become a Participant and participate in such Ecosystem activities if they adhere to the processes and agreements of the Gaia-X Association AISBL.
* Federator of an Ecosystem not federated by Gaia-X AISBL
    * Federators have the option to facilitate an ecosystem by using the available open source Federation Services software but may **not** be 
officially compliant with Gaia-X Policy Rules and follow the conformity and trust processes. An Ecosystem may, for example, provide only a private Catalogue and set up its own criteria for having access to the Ecosystem. Despite this kind of Ecosystem being based on Gaia-X Services and apply the Policy Rules, it cannot be called an official Gaia-X Ecosystem.

## Basic Interactions of Participants

This section describes the basic interaction of the different
Participants as described in the conceptual model (see section 2).

Providers and Consumers within a Ecosystem are identified and well
described through their valid Self-Description, which is initially
created before or during the onboarding process. Providers define their
Service Offerings consisting of Assets and Resources by
Self-Descriptions and publish them in a Catalogue. In turn, Consumers
search for Service Offerings in Gaia-X Catalogues that are coordinated
by Federators. Once the Consumer finds a matching Service Offering in a
Gaia-X Catalogue, the Contract negotiation between Provider and Consumer
determine further conditions under which the Service Instance will be
provided. The Gaia-X association AISBL does not play an intermediary role during the
Contract negotiations but ensures the trustworthiness of all relevant Participants
and Service Offerings.

The following diagram presents the general workflow for Gaia-X service
provisioning and consumption processes. Please note that this overview represents
the current situation and may be subject to changes according to the
Federation Services specification. The specification will provide more
details about the different elements that are part of the concrete
processes.

The Federation Services are visible in the following objects: 

Data Sovereignty Services appear in the mutual agreement and execution of
(Usage) Policies that are defined in a Contract and concern the Data
Asset.

Identity and Trust appears in the onboarding process and ensures the identification and authentication of all Participants.

Compliance is also assured during onboarding and is subject to the 
underlying continuous automated monitoring throughout the lifecycle.

The Federated Catalogue and the Self-Descriptions details the elements that
match Consumers with Providers.

![](figures/image10.png)
*10 Basic Provisioning and Consumption Process*
