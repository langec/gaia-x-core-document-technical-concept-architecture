## Provider

A [Participant](#participant) who provides [Resources](#resource) and [Service Offerings](#service-offering) in the Gaia-X ecosystem.

Note: The service(s) offered by a Provider are cloud and/or Edge services. Thus, the Provider will typically be acting as a Cloud Service Provider (CSP) to their [Consumers](#consumer).
