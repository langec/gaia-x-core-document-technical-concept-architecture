## Policy (technical)

Statements, rules or assertions that specify the correct or expected behavior of an entity.

In the conceptual model, they appear as attributes in all elements related to all [Resources](#resource).

### references
- NIST SP 800-95 Open Grid Services Architecture Glossary of Terms (25 January 2005)
- NISTIR 7621 Rev. 1 NIST SP 800-95	https://csrc.nist.gov/glossary/term/Policy
