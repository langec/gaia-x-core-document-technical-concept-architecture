## Self-Description

A Self-Description expresses characteristics of a [Resource](#resource), [Service Offering](#service-offering) or [Participant](#participant) and describes properties and [Claims](#claim) which are linked to the Identifier.

### alias
- Gaia-X Self-Description
