## End-User

A natural person or process not being a [Principal](#principal), using a digital offering from a [Participant](#participant).
Participants manage their relations with End-Users - including identities - outside of the Gaia-X ecosystem scope.
End-Users have no credentials within the Gaia-X Ecosystem.
