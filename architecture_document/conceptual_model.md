# Gaia-X Conceptual Model

The Gaia-X conceptual model, shown in the figure below, describes all concepts
in the scope of Gaia-X and relations among them. Supplementary,
more detailed models may be created in the future to specify further
aspects. Minimum versions of important core concepts in the form of mandatory attributes for Self-Descriptions
are presented in Appendix [A3](appendix.md#a3). The general interaction pattern is
further described in the section
[Basic Interactions of Participants](usecase.md#basic-interactions-of-participants).

The Gaia-X core concepts are represented in classes. An entity
highlighted in blue shows that an element is part of Gaia-X and
therefore described by a Gaia-X Self-Description. The upper part of the
model shows different actors of Gaia-X, while the lower part shows
elements of commercial trade and the relationship to actors outside
Gaia-X.

![](figures/GAIA-X_models-Main.png)
*Gaia-X conceptual model*

## Participants

A Participant is an entity, as defined in ISO/IEC 24760-1 as "item
relevant for the purpose of operation of a [domain](federation_service.md#identity-and-trust) that has
recognizably distinct existence"[^7], which is onboarded and has a
Gaia-X Self-Description. A Participant can take on one or more of
the following roles: Provider, Consumer, Federator. Section [Federation Services](conceptual_model.md#federation-services)
demonstrates use cases that illustrate how these roles could be filled.
Provider and Consumer present the core roles that are in a
business-to-business relationship while the Federator enables their
interaction.

[^7]: ISO/IEC. IT Security and Privacy — A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO/IEC. <https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en>

### Provider

A Provider is a Participant who provides Resources in the
Gaia-X Ecosystem. The Provider defines the Service Offering including terms and
conditions as well as technical Policies. Furthermore, it provides the
Service Instance that includes a Self-Description and associated
Policies. Therefore, the Provider operates different Resources.

### Federator

Federators are in charge of the Federation Services and the Federation
which are independent of each other. Federators are Gaia-X Participants.
There can be one or more Federators per type of Federation Service.

A Federation refers to a loose set of interacting actors that directly
or indirectly consume, produce, or provide related Resources.

### Consumer

A Consumer is a Participant who searches Service Offerings and consumes
Service Instances in the Gaia-X Ecosystem to enable digital offerings
for End-Users.

## Service Composition

![](figures/GAIA-X_models-Service composition.png)
*Gaia-X conceptual model*

## Resources and Resource Templates

Resources describe in general the goods and objects of a Gaia-X Ecosystem.
A Resource can be a Data Resource, a Software Resource, a Node or an Interconnection.
Each resource is characterized by endpoints and access rights and belongs to a Resource owner.
The different categories of Resources are visualized in Figure 3 and defined below:

```mermaid
 classDiagram
      class Resource{

      }
      class Data Resource{
      }
      class Software Resource{
      }
      class Node{
      }
      class Interconnection{
      }
      Resource <|-- Data Resource
      Resource <|-- Software Resource
      Resource <|-- Node
      Resource <|-- Interconnection

```
*Resource Categories*

A Data Resource consists of data in any form and necessary
information for data sharing.

A Node is a Resource that represents a computational or physical entity
that hosts, manipulates, or interacts with other computational or
physical entities.

A Software Resource is a Resource consisting of non-physical
functions.

An Interconnection is a Resource presenting the connection between two
or more Nodes. These Nodes are usually deployed in different infrastructure 
domains and owned by different stakeholders, such as Consumers and/or Providers.
The Interconnection between the Nodes can be seen as a path which exhibits special 
characteristics, such as latency, bandwidth and security guarantees, that go beyond 
the characteristics of a path over the public Internet.

Resource templates are the entities provided by a Provider to make the Resource available for order.
They depend on the respective Resources and are characterized by a specification, e.g., of data schema, and by usage rights.
Resource templates are therefore used to compose Service Offerings.

### Policies

Policy is defined as a statement of objectives, rules, practices, or regulations
governing the activities of Participants within Gaia-X.
From a technical perspective Policies are statements, rules or
assertions that specify the correct or expected behaviour of an
entity[^9][^10].

The [Policy Rules Document](https://gaia-x.eu/pdf/Gaia-X_Policy%20Rules_Document_2104.pdf) explains the general Policies defined by the Gaia-X association for all
Providers and Service Offerings. They cover, for example, privacy or cybersecurity policies
and are expressed in the conceptual model indirectly via Gaia-X Federation Service Compliance
and as attributes of the Resources, Service Offerings, and Service Instances.

These general Policies form the basis for detailed Policies for a particular Service Offering, 
which can be defined additionally and contain particular restrictions and obligations defined by
the respective Provider or Consumer. They occur either as a Provider Policy (alias Usage Policies)
or as a Consumer Policy (alias Search Policy):

- A Provider Policy/Usage Policy constraints the Consumer's use of a Resource. _For example, a Usage Policy for data can constrain the use of the data by allowing to use it only for x times or for y days._

- A Consumer Policy describes a Consumer's restrictions of a 
requested Resource. _For example, a Consumer gives the restriction that a Provider of a certain service has to fulfil demands such as being located in a particular jurisdiction or fulfil a certain service level._

In the conceptual model, they appear as attributes in all elements related to Resources.
The specific Policies have to be in line with the general Policies in the [Policy Rules Document](https://gaia-x.eu/pdf/Gaia-X_Policy%20Rules_Document_2104.pdf).

[^9]: Singhal, A., Winograd, T., & Scarfone, K. A. (2007). Guide to secure web services: Guide to Secure Web Services - Recommendations of the National Institute of Standards and Technology. Gaithersburg, MD. NIST. <https://csrc.nist.gov/publications/detail/sp/800-95/final https://doi.org/10.6028/NIST.SP.800-95>
[^10]: Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734>

```mermaid
graph TD;
    A[Policy Rules] --> |defines| B[general Gaia-X Policies]
    B --> |basis for| C[Resource-specific Policies]
    C -->D[Provider Policy/ Usage Policy]
    C -->E[Consumer Policy/ Search Policy]
```

## Federation Services

Federation Services are services required for the operational
implementation of a Gaia-X Data Ecosystem. They are explained in greater
detail in the [Federation Service](federation_service.md) section.

They comprise four groups of services that are necessary to enable
Federation of Resources, Participants and interactions between
Ecosystems. The four service groups are Identity and Trust, Federated
Catalogue, Sovereign Data Exchange and Compliance.

## Service Offering

A Service Offering is defined as a set of Resources which a Provider aggregates and publishes as a single entry in a Catalogue. Service Offerings may themselves be aggregated realizing service composition. The instantiation of a Service Offering is the deliverable of a Provider to a Consumer. The Federation Services provide the foundation
for Service Offerings and the Service Offering uses and conforms to the
Federation Services.

## Contract

Gaia-X association is not getting involved into the realisation of the `Contact`. However, in order to ease participants with the establishement and to enter into a contractual relationship, we are defining below a common model for `Contract`.

### Concept: Computable Contracts as a service

- Contracts are the basis for business relationships.
- Whereas a licensor has rights with respect to a resource and is willing to (sub-) license such rights by a defined set of conditions.
- Whereas a licensee would like to get license rights with respect to a resource by a defined set of conditions.
- Licensor and licensee agree on it in form of a contract.
- Every role of the GAIA-X conceptual model as well as of operational model can be seen as legal persons and therefore may have a role as a licensor or licensee or both.
- In traditional centralized driven eco-systems the platform provider which is very often the eco-system owner, defines the contractual framework and participants need to accept without any possibility for negotiation.
- In distributed and federated eco-systems individual contracting becomes much more important to support individual content of contractual relations e.g. individual set of conditions.
- The ability to negotiate on contracts is key for a sovereign participation. The ability to observe if all parties of a contract behave the way it is agreed, to validate their rights, to fulfill their obligations and ensure that no one can misuse information is key for a trustful relationship.
- Computable contracts aim to easy the complex processes of contract design, contract negotiation, contract signing, contract termination as well as to observe the fulfillment of contractual obligations and compliance with national law.

```mermaid
flowchart TB
participant[Legal Person]
op_role[Operational Model roles]
cm_role[Conceptual Model roles]
compliance[Gaia-X Compliance]
licensor[Licensor]
licensee[Licensee]
lic_rights[License rights]
usage[Gaia-X resource usage]
t_c[Terms & Conditions]
contract[Contract]
cc_elts[Computable Contract Elements]
cc[Computable Contract]
ncc[Non computable Contract]

op_role & cm_role -- defines --> participant

participant -- is a --> licensee & licensor

licensee -- requests --> lic_rights
licensor -- owns --> lic_rights

licensee -- accepts --> contract
licensor -- offers --> contract

lic_rights -- determines --> usage


lic_rights -- sub-licensed_by --> t_c

t_c -- defined by --> contract

contract -- represented by --> ncc & cc

cc -- consists of --> cc_elts

cc_elts -- verified by --> compliance
participant -- verified by --> compliance
```

## Additional Concepts 

In addition to those concepts and their relations mentioned above,
further ones exist in the conceptual model that are not directly
governed by Gaia-X. These concepts do not need to undergo any procedures
directly related to Gaia-X, e.g., do not create or maintain a Gaia-X
Self-Description.

First, the Service Instance realizes a Service Offering and can be used
by End-Users while relying on a contractual basis.

Second, Contracts are not in scope of Gaia-X but present the legal basis
for the Services Instances and include specified Policies. Contract
means the binding legal agreement describing a Service Instance and
includes all rights and obligations. This comes in addition to the
automated digital rights management embedded in every entity's
Self-Description.

Further relevant actors exist outside of the Gaia-X scope in terms of
End-Users and Resource Owners.

Resource Owners describe a natural or legal person,
who holds the rights to Resources that will be provided according to
Gaia-X regulations by a Provider and legally enable its provision.
As Resources are bundled into a Service Offering and nested Resource compositions can be possible,
there is no separate resource owner either. Resources can only be realized together 
in a Service Offering and Service Instance by a Provider, which presents no need to model a 
separate legal holder of ownership rights.

End-Users use digital offerings of a Gaia-X Consumer that are enabled by
Gaia-X. The End-User uses the Service Instances containing
Self-Descriptions and Policies.

## Examples

### Personal Finance Management example

This example describes the various Gaia-X concepts using the Open Banking scenario of a Personal Finance Management service (PFM) in SaaS mode.

Let’s suppose that the service is proposed by a company called `MyPFM` to an end user `Jane` who have bank accounts in two banks: Bank<sub>1</sub> and Bank<sub>2</sub>.  
`MyPFM` is using services provided by Bank<sub>1</sub> and Bank<sub>2</sub> to get the banking transactions of `Jane` and then aggregates these bank statements to create Jane’s financial dashboard.

`Jane` is the **End-User**.

Bank<sub>1</sub> and Bank<sub>2</sub> are **Providers** defining the **Service Offerings** delivering the banking transactions and operating the corresponding **Service Instances**. They are also **Resource Owners** for the bank statements, which are **Resources** composing the **Service Offerings** (`Jane` is the data subject as per GDPR[^gdprdatasubject]).  
The associated **Resource Policies** are in fact predefined by the PSD2[^PSD2] directive from the European Parliament.

[^gdprdatasubject]: Rights of the data subject <https://gdpr-info.eu/chapter-3/>
[^PSD2]: Payment services (PSD 2) <https://ec.europa.eu/info/law/payment-services-psd-2-directive-eu-2015-2366_en>

`MyPFM` is the **Consumer** which consumes the **Service Instances** provided by Bank<sub>1</sub> and Bank<sub>2</sub> in order to create a financial dashboard and to offer it to `Jane`.  
`MyPFM` is also likely consuming **Service Instances** from a PaaS **Provider** in order to run its own code, such as dashboard creation.
