# Appendix

## A1

Examples of Attribute Categories per Self-Description in Gaia-X are
discussed in Appendix A.

- **Providers**: Every Provider of Service Offerings has to be
  registered as Provider and thus requires a Self-Description. The
  categories comprise identity, contact information, certification.

- **Nodes:** Self-Descriptions of Nodes describe relevant functional
  and non-functional attributes of Nodes as described in Section
  "Basic Architecture Elements". The Attribute Categories comprise
  availability, connectivity, hardware, monitoring, physical security
  and sustainability.

- **Software Assets:** Self-Descriptions of Software Assets describe
  Software Assets as defined in the [Conceptual Model](conceptual_model.md).
  Attribute Categories for Software Assets are still under discussion
  and are not yet finalized.

- **Consumers (optional)**: Self-Descriptions of Consumers are
  optional, but may be required for accessing critical Data Assets
  and/or specific domains. Attribute categories for Consumers are
  still under discussion and are not yet finalized.

## A2

**Operational example Federated Trust Model**

The Federated Trust Model is currently being updated, the new version will appear here in the next version of this document. 


## A3

This appendix presents minimal core versions of central Gaia-X concepts of the Conceptual Model.
That includes mandatory attributes as well as their types and cardinalities for the core concepts of Participant, its special case Provider, Service Offering, Asset, Data Asset (and Data Service Offering as a special case of Service Offering, representing the service through which a Data Asset is provided), Software Asset, Node, and Interconnection.

### Governance of Mandatory Attributes

The following lists of mandatory attributes reflect the consensus of stakeholder workshops held by the Self-Description Work Package.
The proposer of each mandatory attribute was required to justify why it should be mandatory.

Anticipating further specializations of the Self-Description Schemas, future Architecture Document releases may instead refer to a dedicated, separate Self-Description document and an official Federated Catalogue hosting the Self-Description Schemas that implement the mandatory attributes.
Future changes to the mandatory attributes, including additions, modifications, as well as deprecations, will be handled through the ADR process or a specialization of it.
Mandatory attributes that, in future, are deemed to no longer be mandatory, or to no longer be applicable at all, shall not be deleted, but be deprecated as specified in OWL[^999]

[^999]: OWL 2 Web Ontology Language. Structural Specification and Functional-Style Syntax (Second Edition). W3C Recommendation 11 December 2012. http://www.w3.org/TR/2012/REC-owl2-syntax-20121211/

### Semantics of Mandatory Attributes

The focus of this section is on _mandatory_ attributes, i.e., those for which at least one value must be specified.
Other attributes, which are of interest but optional, are out of scope of this revision of the Architecture Document.
Technically, the presence or absence of a mandatory attribute in a Self-Description is checked by a validation shape.
This validation mechanism only has access to the Self-Description and to the validation shapes, not to an oracle that provides further information about the real world.
Thus, attributes are specified as either mandatory or non-mandatory.
It is not technically possible to specify that an information should be mandatory if a certain situation holds in reality, e.g., that a Provider's parent entity must be specified if the Provider has a parent entity.

For some attributes, it it recommended to use values from Controlled Vocabularies.
Gaia-X specific controlled vocabularies have not yet been standardized.
In future, they may be standardized according to a similar process as the specification of mandatory attributes.
All such URIs given below serve as examples.
The semantics of these terms may be fixed later.

### Participant

Unless mentioned otherwise, the following attributes have identifiers in the _http://w3id.org/gaia-x/participant#_ namespace, abbreviated with the _gax-participant:_ prefix (e.g., _gax-participant:hasLegallyBindingName_).

| Attribute     | Description | Type(s)                      | Cardinality   | Example value |
| ------------- |-----------------------------------------| ------------- | -------------| -------------|
| _hasLegallyBindingName_ | legal name | [xsd:string](http://www.w3.org/2001/XMLSchema#) | 1..1 | "SAP SE" |
| _hasLegallyBindingAddress_ | legal address | [vcard:Address](https://www.w3.org/TR/vcard-rdf/) | 1..1 | (a structured object having, e.g., the attributes _vcard:street-address_, _vcard:locality_ and _vcard:country-name_) |

#### Provider / Consumer

A _Participant_ implicitly becomes a _Provider_ once they
provide (i.e., possess/operate/define) at least one _Asset_, _Resource_ or _ServiceOffering_.
They implicitly become a _Consumer_ once they consume at least one _ServiceInstance_.
To a _Provider_, the following additional mandatory attributes apply:

| Attribute              | Description               | Possible Type(s)          | Cardinality   |  Example Value |
| ---------------------- |------------------------- |-------------------------| -------------| --------------|
| _hasLegalForm_              | legal form                | [xsd:string](http://www.w3.org/2001/XMLSchema#) or controlled vocabulary entry (URI) | 1..1     | https://gaia-x.eu/vocab/legal/Societas_Europeana |
| _hasJurisdiction_           | jurisdiction              | [xsd:string](http://www.w3.org/2001/XMLSchema#) or controlled vocabulary entry, e.g., country (URI) | 1..1 | http://gaia-x.eu/vocab/country/DE |
| _hasSalesTaxID_ | sales tax ID / VAT ID | [xsd:string](http://www.w3.org/2001/XMLSchema#) | 1..1              | "DE 129515865" (in Germany) |
| _hasLegalRegistrationNumber_ | legal registration number | [xsd:string](http://www.w3.org/2001/XMLSchema#) | 1..1              | "HRB 1234" (in Germany) |
| _hasWebAddress_ | web address | [xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI)                                | 1..*          | https://gaia-x.eu/ | 
| _hasIndividualContactLegal_ | contact person for legal purposes | [vcard:Agent](http://www.w3.org/2006/vcard/ns#), [schema:Person](http://schema.org/) | 1..* | (a structured object having, e.g., the attributes _schema:givenName_ and _schema:email_) |
| _hasIndividualContactTechnical_ | contact person for technical purposes | [vcard:Agent](http://www.w3.org/2006/vcard/ns#), [schema:Person](http://schema.org/) | 1..* | (a structured object having, e.g., the attributes _schema:givenName_ and _schema:email_) |

### Service Offering

| Attribute     | Description | Possible Type(s)      | Cardinality   |  Example Value |
| ------------- | ------------- |:-------------------------:| -------------:| -------------- |
| _hasServiceTitle_ | Name of the service | [xsd:string](http://www.w3.org/2001/XMLSchema#string) | 1..1 | "Image classification ML service" |
| _hasServiceDescription_ | A description in natural language | [dct:description](http://purl.org/dc/terms/description) | 1..1 | "An ML service for easily training, deploying, and improving image classifiers." |
| hasKeyword |  | [dcat:keyword](http://www.w3.org/ns/dcat#keyword) | 1..n | "Machine Learning", "Classification"
| providedBy | This service's provider(s) | [gax-participant:Provider](https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/participant/participant.html#Provider) | 1..* | gax:Company-1 |
| hasProvisionType | Provision type | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..1 | "Hybrid" / gax:PrivateProvisioning |
| hasServiceModel  | Service model | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..1 | "IaaS" / "PaaS" / "SaaS" |
| hasWebAddress | URL of the service website | [xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI) | 1..1 | http://example.org/ML-classification-service

#### Asset
As of the 21.09 Architecture Document, Assets are no longer part of the Conceptual Model.
The mandatory attributes for describing the similar concept of Resource are given below.
A further revision of the Resource mandatory attributes, aligned with the ones of the former Asset concept, is expected for 2021-Q4.

| Attribute     | Possible Datatype(s)      | Cardinality   |
| ------------- |:-------------------------:| -------------:|
| name          | [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1          |
| description   | [dct:description](http://purl.org/dc/terms/description)           | 1..1          |
| owned_by      | [foaf:Person](http://xmlns.com/foaf/0.1/)  | 1..*          |

#### Data Asset / Software Asset

Data Asset and Software Asset are subclasses of Asset that do not require additional mandatory attributes.

### Data Service Offering


| Attribute              | Description               | Possible Type(s)      | Cardinality   |  Example Value |
| ---------------------- |:------------------------- |:-------------------------:| -------------:| -------------:|
| _hasServiceTitle_             | Title of the data service featuring a high level description for quick reference. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) | 1..1 | "Example Data" |
| _hasServiceDescription_       | A more detailed description incl. markdown of the data service that contains all information not included in standardized Self Descriptions | [dct:description](http://purl.org/dc/terms/description) | 1..1 | "This data service contains the data resource formerly kept in my data silo." |
| _hasLicense_            | Reference to the license model of the data service | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..1 | "Public Domain", https://creativecommons.org/publicdomain/zero/1.0/, "CC-BY", "No License Specified" |
| _hasCopyrightHolder_        | Reference to the author or copyright holder as name or DID | [xsd:string](http://www.w3.org/2001/XMLSchema#string) | 1..1 | "Satoshi Nakamoto", did:3:bafyreigh5aiij5xltuqcf5n4dcssgzcvg775cef3o2gcd7z7ssbn5w3sae |
| _hasType_              | Type of the data asset, which helps the discovery process. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..1 | "dataset", http://gaia-x.eu/vocab/assetTypes/Algorithm, "container", "video stream", "audio stream", … |
| _wasCreatedOn_ | The timestamp the data service has been created. ISO 8601 format, Coordinated Universal Time. | [xsd:dateTimeStamp](http://www.w3.org/2001/XMLSchema#) | 1..1 | 2021-07-17T00:31:30Z |
| _conformsToStandard_          | Provides information about standards applied, e.g., ISO 10303. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..n | "ISO10303-242:2014", http://gaia-x.eu/vocab/standards/ISO_13567-1_2017 |
| _hasStandardReference_ | Provides a link to the schema or additional details about the underlying standards applied, in case this link is not attached to the representation of the standard in a controlled vocabulary | xsd:anyURI | 1..n | https://www.iso.org/standard/57620.html |


### Example for an Extension of a Data Service Offering (File)

If the underlying resource is a file this might require different attributes than streams or software products. This shall illustrate an example for a simple data service that serves a file that is available for download or computation.

The mandatory attributes are:

| Attribute          | Description               | Possible Type(s)      | Cardinality   |  Example Value |
| ------------------ |:------------------------- |:-------------------------:| -------------:| -------------:|
| _hasContentType_        | File format, could be detected during the listing process and availability check. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..1 | "text/plain", https://www.iana.org/assignments/media-types/application/json |
| _hasLocalURL_           | Endpoint that is used during the publishing process | [xsd:anyURI](http://www.w3.org/2001/XMLSchema#) | 1..1 | https://raw.githubusercontent.com/examplefile.zip |
| _hasEncryptedURL_       | Contains the encrypted URL, which may be used to enable access control. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) | 1..1 | 7f9s79fu90s7f0s8fsfjsdpfjß8a002 |
| _hasEncryptionEndpoint_ | Contains the endpoint responsible for URL decryption and access control | [xsd:anyURI](http://www.w3.org/2001/XMLSchema#) | 1..1 | https://accesscontroller.yourname.com |
| _hasFileIndex_          | Index number, starting from 0 | [xsd:nonNegativeInteger](http://www.w3.org/2001/XMLSchema#) | 1..1 | 4 |
| _hasFileEncoding_       | File encoding (e.g., UTF-8). | [xsd:string](http://www.w3.org/2001/XMLSchema#string) or controlled vocabulary entry (URI) | 1..n | "UTF-8", http://gaia-x.eu/vocab/encoding/US-ASCII |
| _hasContentLength_      | Size of the file in bytes. | [xsd:nonNegativeInteger] | 1..n | 378928719 |
| _hasChecksum_           | Checksum of the file using your preferred format (i.e., MD5). Format is specified in _hasChecksumType_. | [xsd:string](http://www.w3.org/2001/XMLSchema#string) | 1..n | 25d422cc23b44c3bbd7a66c76d52af46 |
| _hasChecksumType_       | Format of the provided checksum. Can vary according to server (i.e., Amazon vs. Azure) | xsd:string | 1..n | md5 |

#### Resource

The mandatory attributes are:

| Attribute     | Possible Datatype(s)      | Cardinality   |
| ------------- |:-------------------------:| -------------:|
| location      | [dct:location](http://purl.org/dc/terms/location)              | 1..1          |
| jurisdiction  | [dct:location](http://purl.org/dc/terms/location)              | 1..1          |
| provided_by   | [gax-participant:Participant](http://w3id.org/gaia-x/participant#Provider)  | 1..*          |

#### Interconnection Resource

An interconnection resource can consist of a physical medium resource, a connection resource or a route resource. One or multiple interconnection resources compose an interconnection service. The mandatory attributes (aka resource templates) of these are listed in separate tables below.

| Attribute              | Description               | Possible Type(s)     | Cardinality |  Example Value |
| ---------------------- |:------------------------- |:--------------------:| -----------:| --------------:|
| _hasPhysicalMediumResource_ |                           | PhysicalMediumResource  | 0..1        |                |
| _hasConnectionResource_  |                           | ConnectionResource   | 0..1        |                |
| _hasRouteResource_          |                           | RouteResource           | 0..1        |                |

##### Physical Medium Resource

The Physical Medium Resource can be an individual interconnection resource. It has the following mandatory attributes:

| Attribute                      | Description               | Possible Type(s)      | Cardinality   |  Example Value |
| ------------------------------ |:------------------------- |:-------------------------:| -------------:| --------------:|
| _hasPhysicalMediumResourceLocation_ | references two locations (A and B) that require a connection (physical or virtual)  | [xsd:string](http://www.w3.org/2001/XMLSchema#) or [dct:Location](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) | 2..2          | (a point of presence, a facility, a data center, or an address) |
| _hasPhysicalMediumResourceType_         | the type of technology used for physical connection                | [xsd:string](http://www.w3.org/2001/XMLSchema#) or controlled vocabulary entry (URI)                          | 1..1          |       "copper cable", http://gaia-x.eu/vocab/medium/WiFi, "fiber", "4G/5G", …         |

##### Connection Resource

A Connection Resource (for example an Ethernet or Dense Wavelength Division Multiplexing (DWDM)) can also be the interconnection resource on its own. It has the following mandatory attributes:

| Attribute              | Description               | Possible Type(s)     | Cardinality |  Example Value |
| ---------------------- |:------------------------- |:--------------------:| -----------:| --------------:|
| _hasConnectionPointA_                       |    source reference ID                       |      [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1        |       MAC address,  VLAN ID, …         |
| _hasConnectionPointZ_                        |         destination reference ID                  |      [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1        |    MAC address,  VLAN ID, … |
| _hasBandwidth_                |         contractual bandwidth defined in the service level agreement (SLA)  |      measure, e.g., in unit Gbps | 1..1 | 1 Gbps, 10 Gbps, 100 Gbps, 400 Gbps |
| _hasLatency_              |           contractual latency defined in the SLA.  If not specified, then best effort is assumed.                |     measure in the dimension of time, or the controlled vocabulary entry "best effort" | 1..1        |       1 s, 10 ms, http://gaia-x.eu/vocab/value/BestEffort |
|      _hasAvailability_             |       contractual availability of provider services defined in the SLA agreement. If not specified, then best effort is assumed.                   |  measure in the pseudo-unit percent, or the controlled vocabulary entry "best effort"  | 1..1        |       99.9%, 99.99%, 99.999%,n/a|
|     _hasPacketLoss_              |          contractual packet loss defined in the SLA. If not specified, then best effort is assumed. |               measure in the pseudo-unit percent, or the controlled vocabulary entry "best effort"  | 1..1        |       1%, 10%,  http://gaia-x.eu/vocab/value/BestEffort   |
|      _notsupportedProtocols_             |       not supported protocols among used layers should be specified |      [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1        |       the spanning tree protocol is not supported |
|(optional) _hasIntermediateConnectionPointsN_                       |   intermediate connection/ interconnection point ID                       |      [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..N        |       MAC address,  VLAN ID, …         |
| (optional)_hasConnectionType_                       |   point-to-point, one-to-many, many-to-many, many-to-one                      |      [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1        |      ethernet unicast, multicast, broadcast support       |

###### Measure

A measure is a compound of a numeric value and a unit of measurement.  A measure is typically not given an identifier and reused in multiple places, but written down locally, as the value of a single attribute.  I.e., in the RDF data model, it is typically represented as a blank node.  It has the following mandatory attributes:

| Attribute              | Description               | Possible Type(s)     | Cardinality |  Example Value |
| ---------------------- |:------------------------- |:--------------------:| -----------:| --------------:|
| _hasValue_ | the value of the measure | any numeric datatype, e.g., [xsd:float](http://www.w3.org/2001/XMLSchema#) | 1..1 | 100 |
| _hasUnit_ | the unit of measurement | [xsd:string](http://www.w3.org/2001/XMLSchema#) or controlled vocabulary entry (URI) | 1..1 | http://gaia-x.eu/vocab/units/Gbps |

Controlled vocabularies of units support the definition of compound units from base units, the provision of conversion factors, etc.  Vocabularies suitable for reuse include QUDT[^1].

[^1]: http://www.qudt.org/

##### Route Resource

A Route Resource can be an interconnection resource. It has the following mandatory attributes:

| Attribute              | Description               | Possible Type(s)     | Cardinality |  Example Value |
| ---------------------- |:------------------------- |:--------------------:| -----------:| --------------:|
|  connected network                      |      autonomous system (AS) number (ASN) should be provided                     |          xsd:integer            | 1..1        |        200, 714        |
| _prefix set_                       |         CIDR Provider Identifier          |      [xsd:string]                | 1..1        |     10.1.1.1/24    |
|        origin node              |          reference to connection points            |        xsd:string              | 1..1        |      Node ID     |
