Architecture Decision Record Process
====================================

:status: draft

.. figure:: adr-process.png
   :figwidth: 7cm

According to the Articles of Association of the GAIA-X Foundation, the Board of
Directors shall set up a Policy Rules Committee and a Technical Committee. The
Board of Directors can set up and delegate powers to additional working groups,
expert (technical) committees and other initiatives.

The Technical Committee of the GAIA-X Foundation has the power to accept or
reject ADR.

ADR are developed and proposed by the technical working groups set up by the
Board of Directors within the GAIA-X Foundation, as well as the working groups
that are active in regional and topical "hubs" within the larger GAIA-X
community.

An ADR draft is disseminated for feedback to the mailing list used for the
technical development of the specification (currently gaia-x@priv.ovh.net).

The feedback time for the draft version is at least 7 days. When a consensus is
reached within the community, the ADR is proposed to the Technical Committee by
sending it to the convener of the Technical Committee.

When an ADR is proposed, it is given a number-identifier for future reference
and is added to the repository (currently at
https://gitlab.com/gaia-x/gaia-x-core/gaia-x-core-document-technical-concept-architecture/-/tree/master/architecture_decision_records).

The Technical Committee comes to a conclusion within 6 weeks of the ADR being
proposed. Decisions must be taken with a 2/3 majority. If no conclusion is
reached within 6 weeks, then the ADR is rejected. If an ADR is rejected, the
Technical Comittee responds with a rationale where it explains the decision.

The status of an accepted or rejected ADR is updated in the repository.
The status change is further communicated over the mailing list.

The accepted ADR are added to the Architecture Document in an Appendix. The
definitions and technical details of the Architecture Document are corrected in
order to reflect the spirit of the accepted ADR.

Status and Lifecycle of an ADR
------------------------------

draft
   The ADR is still work in progress and has not been proposed by a Work Package
   or several work packages.

proposed
   The ADR has been proposed, but there is no final decision on it so far.

rejects
   The ADR has been rejected.

accepted
   The ADR has been accepted.

deprecated
   The ADR was once accepted but is no longer effective.

superseded
   The ADR was once accepted. But the content has been superseded by a more
   recent ADR.

Project Decision Records
------------------------

Projects within GAIA-X, such as the "minimum viable GAIA" implementation can
follow the ADR process in a more lightweight manner. The so-called PDR do not
require acceptance by the Technical Committee.

If a PDR is to be transformed into an ADR, it will have draft-status and is
disseminated and commented on the mailing-list as if it was a new ADR.
