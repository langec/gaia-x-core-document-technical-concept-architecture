#!/bin/sh
set -ex

./architecture_document/glossary/build_glossary.sh
if [ ! -z "${GITLAB_HOST}" ]; then
    ./doc-highlight-mr.py;
fi
