# Self-Descriptions 

## Self-Description Structure

JSON-LD, VC, etc.

## References between Self-Descriptions

Identifiers, etc.

## Self-Description Schemas

Hierarchy of Self-Descriptions
Multiple Inheritance

## Lifecycle and Versioning

- Stakeholders
  
  - Issuer vs. Owner vs. Provider
  - Federators
  - Certification Assessment Bodies

- Lifecycle Changes

  - Creation
  - Timeout
  - Deprecation
  - Revocation

## Integrity Checks and Validation

- Syntax
- Semantics
- Trust

When are these checks performed / repeated?
Can the references between self-descriptions trigger rechecks?